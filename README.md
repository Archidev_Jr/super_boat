# \Super_Boat/
Conception full-stack avec Angular, Django, Django Rest Framework et Serverless.
Base de données NoSQL avec DynamoDB et SQLite3 pour Django.
Le design est fait avec Angular Material.

# Description
Projet d'étude visant à évaluer nos compétences avec la conception d'un back office.
Ce site web est destiné au gérant d'un point de revente des produits de Bateau Thibault.

## Avant de lancer le projet / Création de fichier .env et credentials.

Créer un nouveau fichier environnemental (.env) dans le dossier dev_ops:
```env
# On utilise le fichier environemental pour surcharger les variables d'environnement dans le dossier /dev_ops.
PEPPER=poivre
PRIVATE_KEY=clebienprivee
```
Pour le fichier credentials d'AWS:  
(~/.aws/credentials)
```
[default]
aws_access_key_id = ""
aws_secret_access_key = ""
```

Faire les commandes suivantes afin de pouvoir lancer le projet:   
(Dans chaque dossier front_end dev_ops et python_drf)

```bash
make
```

Et voilà! Tu es paré pour lancer l'application!


## Technologies
Framework:

- Angular (Front)
- Django (API Rest)
- Django Rest Framework (API Rest)
- Serverless (API Rest)

Autres technologies:

- DynamoDB (NoSQL avec Serverless)
- SQLite3 (Django)
- JSON Web Token (JWT)
- SVG (non présent sur le code existant)
- Angular Material (CSS)


## Progression
En cours de développement.

## L'auteur
Adel MEDJDOUB (it's me)

## Sources
[La documentation officielle de Serverless](https://www.serverless.com/)    
[Le cours d'Arthur ESCRIOU (AWS)](https://github.com/arthurescriou/crud-sls-dynamoDB/)    
Le cours du professeur Bin-Minh (Django).  
Le cours de Tomas MIZRAHI.  
[La documentation officielle de Django Rest Framework](https://www.django-rest-framework.org/)    
[La documentation officielle de Serverless](https://docs.aws.amazon.com/fr_fr/amazondynamodb/latest/developerguide/workbench.Modeler.CreateNew.html)  
[Markdown Guide](https://www.markdownguide.org/cheat-sheet/)