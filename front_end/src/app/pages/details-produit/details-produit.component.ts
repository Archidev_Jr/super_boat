import { Component, OnInit } from '@angular/core';
import { Products } from 'src/app/core/interfaces/products';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
	selector: 'app-details-produit',
	templateUrl: './details-produit.component.html',
	styleUrls: ['./details-produit.component.css']
})
export class DetailsProduitComponent implements OnInit {

	productsList: Products[] = []

	constructor(private productService: ProductsService) { }

	ngOnInit(): void {
		this.productService.getProducts().subscribe(res => {
			this.productsList = res;
			console.log(this.productsList);
		});
	}
}
