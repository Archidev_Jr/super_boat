export interface Products {
    id : number;
    name: string;
    sale: boolean;
    category: boolean;
    availability: boolean;
    price: number;
    price_on_sale: number;
    comments: string;
    discount: number;
    owner: string;
    quantity_stock: number;
    quantity_sold: number;
}
