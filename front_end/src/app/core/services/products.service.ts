import { Injectable } from '@angular/core';
import { Products } from '../interfaces/products';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http : HttpClient) { 
    
  }
  
  getProducts(){
    return this.http.get<Products[]>("../../../assets/data/products.json");
  }

}
