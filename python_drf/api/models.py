from django.db import models
from uuid import uuid4

class Produit(models.Model):
	uuid = models.UUIDField(primary_key = True, default = uuid4, editable = False)
	comments = models.CharField(blank = True, max_length = 255)
	category = models.IntegerField()
	availability = models.BooleanField()
	price = models.IntegerField()
	price_on_sale = models.IntegerField()
	discount = models.IntegerField()
	sale = models.BooleanField()
	owner = models.CharField(max_length = 32)
	name = models.CharField(max_length = 64)
	quantity_stock = models.IntegerField()
	quantity_sold = models.IntegerField()

	class Meta:
		ordering = ['name']
