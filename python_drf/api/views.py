from django.http import JsonResponse
from rest_framework import viewsets
from .models import Produit
from .serializers import ProduitSerializer

def api_home(request, *args, **kwargs):
	print(request)
	return JsonResponse({"body": "Hello world !"})

class ProduitViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows produits to be viewed or edited
	"""
	queryset = Produit.objects.all()
	serializer_class = ProduitSerializer
