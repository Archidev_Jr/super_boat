from rest_framework import serializers
from .models import Produit

class ProduitSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Produit
		fields = ['uuid', 'comments', 'category', 'availability', 'price', 'price_on_sale',
			'discount', 'sale', 'owner', 'name', 'quantity_stock', 'quantity_sold']
