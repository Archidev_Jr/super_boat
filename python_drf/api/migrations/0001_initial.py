# Generated by Django 4.0.3 on 2022-04-03 10:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Produit',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField()),
                ('comments', models.CharField(blank=True, max_length=255)),
                ('category', models.IntegerField()),
                ('availability', models.BooleanField()),
                ('price', models.IntegerField()),
                ('price_on_sale', models.IntegerField()),
                ('discount', models.IntegerField()),
                ('sale', models.BooleanField()),
                ('owner', models.CharField(max_length=32)),
                ('name', models.CharField(max_length=64)),
                ('quantity_stock', models.IntegerField()),
                ('quantity_sold', models.IntegerField()),
            ],
            options={
                'ordering': ['name'],
            },
        ),
    ]
