# Generated by Django 4.0.3 on 2022-04-03 10:46

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_remove_produit_uuid'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='produit',
            name='id',
        ),
        migrations.AddField(
            model_name='produit',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False),
        ),
    ]
