const { v4: uuid } = require("uuid");
const bcrypt = require("bcryptjs");
const AWS = require("aws-sdk");
const { generate, verify } = require("./jwt");

AWS.config.update({
	region: "us-west-1",
	endpoint: "http://localhost:2000",
});

const dynamodb = new AWS.DynamoDB();
const docClient = new AWS.DynamoDB.DocumentClient();

const listTables = () => dynamodb.listTables({}).promise();
const createTable = (name) =>
	dynamodb
		.createTable({
			TableName: name,
			KeySchema: [{ AttributeName: "id", KeyType: "HASH" }],
			AttributeDefinitions: [{ AttributeName: "id", AttributeType: "S" }],
			ProvisionedThroughput: {
				ReadCapacityUnits: 10,
				WriteCapacityUnits: 10,
			},
		})
		.promise();

const methods = {
	create: "create",
	read: "read",
	update: "update",
	delete: "delete",
};

const create = (table, Item) => docClient.put({ TableName: table, Item }).promise();
const read = (table, id) => docClient.get({ TableName: table, Key: { id } }).promise();
const deleteItem = (table, id) => docClient.delete({ TableName: table, Key: { id } }).promise();
const checkExists = async (tableName, id) => (await read(tableName, id)).Item != undefined;
const list = (table) => docClient.scan({ TableName: table }).promise();
const scanTableEQ = async (TableName, propName, propValue) =>
	await docClient
		.scan({
			TableName,
			FilterExpression: "#propValue = :providedValue",
			ExpressionAttributeNames: {
				"#propValue": propName,
			},
			ExpressionAttributeValues: {
				":providedValue": propValue,
			},
		})
		.promise();

const hashingSandP = (mdpLambda, salt) => bcrypt.hashSync(mdpLambda + process.env.PEPPER, salt);
module.exports.hashingSandP = hashingSandP;

module.exports.checkString = (obj, propName, formattedName) => {
	if (!Object.keys(obj).includes(propName))
		return {
			statusCode: 400,
			body: `No ${formattedName} provided`,
		};

	if (typeof obj[propName] != "string")
		return {
			statusCode: 400,
			body: `Invalid ${formattedName} type`,
		};

	if (obj[propName].length == 0)
		return {
			statusCode: 400,
			body: `Invalid ${formattedName}`,
		};

	return true;
};
checkString = module.exports.checkString;

module.exports.createUser = async (event) => {
	const credentials = JSON.parse(event.body);
	const { TableNames } = await listTables();

	if (!TableNames.includes("user")) await createTable("user");

	let san = checkString(credentials, "login", "Login");
	if (san !== true) return san;

	san = checkString(credentials, "mdp", "MDP");
	if (san !== true) return san;

	if ((await scanTableEQ("user", "login", credentials.login)).Items.length != 0)
		return {
			statusCode: 400,
			body: "User already exists !",
		};

	const salt = bcrypt.genSaltSync();
	const userL = {
		login: credentials.login,
		mdp: hashingSandP(credentials.mdp, salt),
		token: generate(credentials.login),
		id: uuid(),
		salt,
		role: "user",
	};
	await create("user", userL);
	return {
		statusCode: 201,
		body: userL.token,
	};
};

module.exports.deleteUser = async (event) => {
	try {
		const body = JSON.parse(event.body);
		const { TableNames } = await listTables();

		if (!TableNames.includes("user"))
			return {
				statusCode: 501,
				body: "No table available",
			};

		const san = checkString(body, "id", "ID");
		if (san !== true) return san;

		if (!(await this.userExist(event)))
			return {
				statusCode: 400,
				body: "This user doesn't exist",
			};

		await deleteItem("user", body.id);
		return { statusCode: 200, body: "Success" };
	} catch (ex) {
		const errMsg = `Serverside error in userExist : ${ex}`;
		console.error(errMsg);
		return {
			statusCode: 500,
			body: errMsg,
		};
	}
};

module.exports.userExist = async (event) => {
	try {
		const body = JSON.parse(event.body);
		const { TableNames } = await listTables();

		if (!TableNames.includes("user"))
			return {
				statusCode: 501,
				body: "No table available",
			};

		const san = checkString(body, "id", "ID");
		if (san !== true) return san;

		return await checkExists("user", body.id);
	} catch (ex) {
		const errMsg = `Serverside error in userExist : ${ex}`;
		console.error(errMsg);
		return {
			statusCode: 500,
			body: errMsg,
		};
	}
};

module.exports.crud = async (event) => {
	try {
		//verify(event.headers.Authorizations)
	} catch (e) {
		return { statusCode: 403, body: "Unauthorized" };
	}
	//const { method: httpMethod } = event.requestContext.http
	const { entity, method, id } = event.pathParameters;
	const { TableNames } = await listTables();

	if (!TableNames.includes(entity)) {
		await createTable(entity);
	}
	let response = {};
	if (id) {
		switch (method) {
			case methods.create:
				const item = { id };
				await create(entity, item);
				response = item;
				break;
			case methods.read:
				response = await read(entity, id);
				break;
			case methods.delete:
				response = await deleteItem(entity, id);
				break;
			default:
				response = "Unknown method";
		}
	} else response = await list(entity);

	return {
		statusCode: 200,
		body: JSON.stringify({ event: response }, null, 2),
	};
};

module.exports.isAdmin = async (event) => {
	try {
		// Removing the 'Bearer ' substring then verifying
		const token = event.headers.Authorization.slice(7);
		verify(token);
		return (await scanTableEQ("user", "token", token)).Items[0].role == "admin";
	} catch (e) {
		return false;
	}
};

module.exports.grantAdmin = async (event) => {
	if (!await this.isAdmin(event)) return { statusCode: 403, body: "Unauthorized" };

	const body = JSON.parse(event.body);

	let san = checkString(body, "id", "ID");
	if (san !== true) return san;

	if (!checkExists("user", body.id))
		return {
			statusCode: 400,
			body: "User doesn't exists",
		};

	await docClient
		.update({
			TableName: "user",
			Key: { id: body.id },
			UpdateExpression: "set #role = :r",
			ExpressionAttributeNames: { "#role": "role" },
			ExpressionAttributeValues: { ":r": "admin" },
		})
		.promise();
	return { statusCode: 200, body: `User with ID:${body.id} granted admin privileges successfully` };
};

module.exports.login = async (event) => {
	const credentials = JSON.parse(event.body);

	let san = checkString(credentials, "login", "Login");
	if (san !== true) return san;

	san = checkString(credentials, "mdp", "mdp");
	if (san !== true) return san;

	const result = await scanTableEQ("user", "login", credentials.login);
	if (result.Items.length == 0)
		return {
			statusCode: 400,
			body: "User doesn't exists",
		};

	const mdp = hashingSandP(credentials.mdp, result.Items[0].salt);
	if (result.Items[0].mdp != mdp)
		return {
			statusCode: 403,
			body: "Invalid password",
		};

	const token = generate(credentials.login);
	await docClient
		.update({
			TableName: "user",
			Key: { id: result.Items[0].id },
			UpdateExpression: "set #token = :t",
			ExpressionAttributeNames: { "#token": "token" },
			ExpressionAttributeValues: { ":t": token },
		})
		.promise();
	return { statusCode: 200, body: token };
};
