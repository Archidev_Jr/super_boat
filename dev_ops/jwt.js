const jwt = require("jsonwebtoken");

const generateToken = (value) => jwt.sign({ ...value }, process.env.PRIVATE_KEY, { expiresIn: 60 * 60 });
const verifyToken = (token) => jwt.verify(token, process.env.PRIVATE_KEY);

module.exports.generate = generateToken;
module.exports.verify = verifyToken;
